# Eating Pigeons FMP DAC

**-- One sentence describing the project**
The project Eating Pigeons explores is centred upon the theory of ‘mutual use’ as explored by Val Plumwood.
The project recruits a band of city pigeons in order to create a visual work based on datasets collected and curated through interaction and exchange with local ecosystems.

**-- One sentence describing the computational aspect of the project**
The project will consist of recording city pigeons, input this audio into a program that will extract values from this file. 
The values will be used as a dataset stored in a type of array that will then be used to create a collage of generative activity on screen, this generative image will be displayed on the web.  For this I will create a website to house the piece. 

**--- what programming language**
The computation aspect will be written in either Javascript or C++, and will be a web-based generative visual piece.


**End of relevent information**

--------------------------------------------------------
**Not there yet with these aspects**

--- what code libraries
--- what techniques
--- what algorithms
--- what tutorials you will draw upon
--- what data set

3 - Please provide one code snippet and push it to the repository. This can be a fragment, but should be code that can be run. In the comments, please say how to run it

4 - Draw a diagram that shows the technology architecture of the piece. This can be as a series of blocks, or bricks that make up the technical component of your work, from low level to high level. It should show the functional relationship between the blocks. It should not show non-technical elements such as content or narrative. Keep it simple. Upload this to the repository as a PDF file
